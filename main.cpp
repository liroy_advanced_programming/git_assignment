#include "threads.h"



int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);  //Checks if there is a memory leak

	call_I_Love_Threads();
	
	vector<int> primes1;
	getPrimes(58, 100, primes1);
	
	vector<int> primes3 = callGetPrimes(0, 1000);
	primes3 = callGetPrimes(0, 100000);
	primes3 = callGetPrimes(0, 1000000);
	
	try
	{
		callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	}
	catch (invalid_argument& exception)
	{
		std::cout << exception.what() << std::endl;
	}
	try
	{
		callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 2);
	}
	catch (invalid_argument& exception)
	{
		std::cout << exception.what() << std::endl;
	}
	try
	{
		callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 2);
	}
	catch (invalid_argument& exception)
	{
		std::cout << exception.what() << std::endl;
	}

	system("pause");
	return 0;
}