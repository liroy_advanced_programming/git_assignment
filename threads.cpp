#include <thread>
#include <time.h>
#include "threads.h"

#define INITIAL -1

/*
The function prints "I love threads"
input: none
output: none
*/
void I_Love_Threads()
{
	std::cout << "I Love Threads!" << std::endl;
}
/*
The function creates a thread which calls I_Love_Threads function
input: none
output: none
*/
void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}
/*
The function checks which numbers are primes in a certain range
input: the beginning nnumber range, the end number range and vector of primes number
output: none
*/
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int temp = 0;

	if (end < begin)  //Checks if we got valid inputs
	{
		temp = begin;
		begin = end;
		end = temp;
	}
	 

	for (int i = begin; i <= end; i++)  //Moves on all the numbers in this specific range
	{
		temp = i;

		if (temp == 0 || temp == 1 || temp == INITIAL) //Checks if we got zero (we want avoid division by zero) or 1 or -1
		{
			primes.push_back(i);
		}
		else
		{
			while ((i%temp != 0 && temp != 1 && temp != INITIAL) || temp == i)  //Checks if it is an initial number
			{
				if (i < INITIAL)  //Checks if it is a negative number
				{
					temp++;
				}
				else if (i > 1)  //Checks if it is a positive number
				{
					temp--;
				}
			}
			if (temp == 1 || temp == INITIAL)  //Pushes into the vector the initial number
			{
				primes.push_back(i);
			}
		}
	}
}
/*
The function prints all the values in the vector
input: an integer vector
output: none
*/
void printVector(vector<int> primes)
{
	std::vector<int>::iterator it;
	
	std::cout << "The vector contains:" << std::endl;
	for (it = primes.begin(); it != primes.end(); it++)  //Moves all over the vector
	{
		std::cout << *it << std::endl;
	}
}
/*
The creates a thread which calls to the get Primes function
input: the beginning and the end of the range
output: the vector with the prime number
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> initialNumbersVector;
	const clock_t beginTime = clock();  //Checks when we start

	std::thread myThread(getPrimes, begin, end, std::ref(initialNumbersVector));
	myThread.join();

	std::cout << "The function was active for ";
	std::cout << float(clock() - beginTime) / CLOCKS_PER_SEC;
	std::cout << " seconds." << std::endl;

	return initialNumbersVector;
}
/*
The function writes the prime numbers to file
input: the beginning and the end of the range and the a file to write in
output: none
*/
void writePrimesToFile(int begin, int end, ofstream& file)
{
	int temp = 0;

	if (end < begin)  //Checks if we got valid inputs
	{
		temp = begin;
		begin = end;
		end = temp;
	}

	for (int i = begin; i <= end; i++)  //Moves on all the numbers in this specific range
	{
		temp = i;

		if (temp == 0 || temp == 1 || temp == INITIAL) //Checks if we got zero (we want avoid division by zero) or 1 or -1
		{
			file << i << std::endl;
		}
		else
		{
			while ((i%temp != 0 && temp != 1 && temp != INITIAL) || temp == i)  //Checks if it is an initial number
			{
				if (i < INITIAL)  //Checks if it is a negative number
				{
					temp++;
				}
				else if (i > 1)  //Checks if it is a positive number
				{
					temp--;
				}
			}
			if (temp == 1 || temp == INITIAL)  //Pushes into the vector the initial number
			{
				file << i << std::endl;
			}
		}
	}
}
/*
The function creates a thread which calls to primes to file function
input: the beginning and the end of the range, a file path and a small range
output: none
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int rest = 0;
	int distance = 0;
	int currBegin = 0;
	int currEnd = 0;
	ofstream primesFile(filePath);

	if (N < 0)  //Checks if the input is valid
	{
		throw invalid_argument("Error! N must be greater than 0!!");
	}

	distance = (end - begin) / N;
	rest = (end - begin) % N;
	currBegin = begin;
	currEnd = begin + distance;

	if (primesFile.is_open()) //Checks if we can open the file
	{
		primesFile.open(filePath);  //Opens the file
		primesFile.clear();  //Moves to the beginning of the file
		std::thread* myThreads = new std::thread[std::sqrt(N)];

		const clock_t beginTime = clock();  //Checks when we start

		for (int i = 0; i < std::sqrt(N); i++)  //Creates N threads
		{
			if (i == std::sqrt(N) - 1)  //Checks if it is the last round
			{
				currEnd = currEnd + rest;
			}
			myThreads[i] = std::thread(writePrimesToFile, currBegin, currEnd, ref(primesFile));
			currBegin = currBegin + distance;
			currEnd = currEnd + distance;
		}
		for (int i = 0; i < std::sqrt(N); i++)  //Makes all the threads to do join
		{
			myThreads[i].join();
		}

		std::cout << "The function was active for ";
		std::cout << float(clock() - beginTime) / CLOCKS_PER_SEC;  //Caculates the time when the function was active
		std::cout << " seconds." << std::endl;
		delete[] myThreads;
		primesFile.close();  //Closes the file
	}
	else
	{
		throw invalid_argument("Error! Can't open the file!!");
	}
}